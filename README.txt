About
=====
This module can be used to send discount coupons for best customers 
in Ubercart stores.

To use this module, first coupon code should be configured and generated using
any suitable ubercart discount modules (eg: Ubercart Discount Coupons or 
Ubercart Discounts (Alternative) ).

Configure the coupon code in such a way that it work for multiple users.

This module list out stores best customers based on their purchased order.
From there admin can enter the coupon code and can send to selected users.

After installation,visit the link yoursitename.com/admin/store/best_customers
