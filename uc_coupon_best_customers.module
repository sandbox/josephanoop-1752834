<?php

/**
 * @file
 * Send coupons to best customers in ubercart.
 */

/**
 * Implements hook_menu().
 */
function uc_coupon_best_customers_menu() {
  $items['admin/store/best_customers'] = array(
    'page callback' => 'drupal_get_form',
    'title' => 'Send Coupons To Best Customers',
    'page arguments' => array('uc_coupon_best_customers_form'),
    'access arguments' => array('administer store'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_theme().
 */
function uc_coupon_best_customers_theme() {
  return array(
    'my_form_theme' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Display the best customer report and form.
 */
function uc_coupon_best_customers_form() {
  $form['coupon_code'] = array(
    '#type' => 'textfield',
    '#default_value ' => '',
    '#title' => t('Enter coupon code to send'),
    '#size' => 25,
    '#required' => TRUE,
    '#description' => t('Note: You should have created and configured your coupon code with necessary options using suitable ubercarts discount module'),
    '#weight' => 0,
  );
  $first_name = ($address_preference == 'billing') ? 'billing_first_name' : 'delivery_first_name';
  $last_name = ($address_preference == 'billing') ? 'billing_last_name' : 'delivery_last_name';
  $page = (isset($_GET['page'])) ? intval($_GET['page']) : 0;
  $page_size = (isset($_GET['nopage'])) ? UC_REPORTS_MAX_RECORDS : variable_get('uc_reports_table_size', 30);
  if (!$page_size) {
    $page_size = 100;
  };
  $order_statuses = _uc_reports_order_statuses();
  $rows = array();

  $header = array(
    array('data' => t('#')),
    array('data' => t('Customer'), 'field' => "ou.$last_name"),
    array('data' => t('Username'), 'field' => "u.name"),
    array('data' => t('Orders'), 'field' => 'orders'),
    array('data' => t('Products'), 'field' => 'products'),
    array('data' => t('Total'), 'field' => 'total', 'sort' => 'desc'),
    array('data' => t('Average'), 'field' => 'average'),
  );
  $sql = '';
  $sql_count = '';
  // The following queries are taken from ubercart uc_reports module.
  switch ($GLOBALS['db_type']) {
    case 'mysqli':
    case 'mysql':
      $sql = "SELECT u.uid, u.name, ou.$first_name, ou.$last_name, (SELECT COUNT(DISTINCT(order_id)) FROM {uc_orders} as o WHERE o.uid = u.uid AND o.order_status IN $order_statuses) as orders, (SELECT SUM(qty) FROM {uc_order_products} as ps LEFT JOIN {uc_orders} as os ON ps.order_id = os.order_id WHERE os.order_status IN $order_statuses AND os.uid = u.uid) as products, (SELECT SUM(ot.order_total) FROM {uc_orders} as ot WHERE ot.uid = u.uid AND ot.order_status IN $order_statuses) as total, ROUND((SELECT SUM(ot.order_total) FROM {uc_orders} as ot WHERE ot.uid = u.uid AND ot.order_status IN $order_statuses)/(SELECT COUNT(DISTINCT(order_id)) FROM {uc_orders} as o WHERE o.uid = u.uid AND o.order_status IN $order_statuses), 2) as average FROM {users} as u LEFT JOIN {uc_orders} as ou ON u.uid = ou.uid WHERE u.uid > 0 GROUP BY u.uid";
      $sql_count = "SELECT COUNT(DISTINCT(u.uid)) FROM {users} as u LEFT JOIN {uc_orders} as ou ON u.uid = ou.uid WHERE u.uid > 0";
      break;

    case 'pgsql':
      $sql = "SELECT u.uid, u.name, ou.$first_name, ou.$last_name, (SELECT COUNT(DISTINCT(order_id)) FROM {uc_orders} as o WHERE o.uid = u.uid AND o.order_status IN $order_statuses) as orders, (SELECT SUM(qty) FROM {uc_order_products} as ps LEFT JOIN {uc_orders} as os ON ps.order_id = os.order_id WHERE os.order_status IN $order_statuses AND os.uid = u.uid) as products, (SELECT SUM(ot.order_total) FROM {uc_orders} as ot WHERE ot.uid = u.uid AND ot.order_status IN $order_statuses) as total, ROUND((SELECT SUM(ot.order_total) FROM {uc_orders} as ot WHERE ot.uid = u.uid AND ot.order_status IN $order_statuses)/(SELECT COUNT(DISTINCT(order_id)) FROM {uc_orders} as o WHERE o.uid = u.uid AND o.order_status IN $order_statuses), 2) as average FROM {users} as u LEFT JOIN {uc_orders} as ou ON u.uid = ou.uid WHERE u.uid > 0 GROUP BY u.uid, u.name, ou.$first_name, ou.$last_name";
      $sql_count = "SELECT COUNT(DISTINCT(u.uid)) FROM {users} as u LEFT JOIN {uc_orders} as ou ON u.uid = ou.uid WHERE u.uid > 0";
      break;
  }

  $context = array(
    'revision' => 'formatted-original',
    'type' => 'amount',
  );

  $customers = pager_query($sql . tablesort_sql($header), $page_size, 0, $sql_count);
  $sl_no = 1;
  while ($customer = db_fetch_array($customers)) {
    $name = (!empty($customer[$last_name]) || !empty($customer[$first_name])) ? l($customer[$last_name] . ', ' . $customer[$first_name], 'admin/store/customers/orders/' . $customer['uid']) : l($customer['name'], 'admin/store/customers/orders/' . $customer['uid']);
    $customer_number = ($page * variable_get('uc_reports_table_size', 30)) + (count($rows) + 1);
    $customer_order_name = (!empty($customer[$last_name]) || !empty($customer[$first_name])) ? $customer[$last_name] . ', ' . $customer[$first_name] : $customer['name'];
    $customer_name = $customer['name'];
    $orders = (!empty($customer['orders']) ? $customer['orders'] : 0);
    $products = (!empty($customer['products']) ? $customer['products'] : 0);
    $total_revenue = uc_price($customer['total'], $context);
    $average_revenue = uc_price($customer['average'], $context);

    if ($orders != 0) {
      $checkboxes[$customer['uid']] = '';
      $form[$customer['uid']]['no'] = array('#value' => $sl_no);
      $form[$customer['uid']]['name'] = array('#value' => $name);
      $form[$customer['uid']]['username'] = array('#value' => $customer_name);
      $form[$customer['uid']]['orders'] = array('#value' => $orders);
      $form[$customer['uid']]['products'] = array('#value' => $products);
      $form[$customer['uid']]['total'] = array('#value' => $total_revenue);
      $form[$customer['uid']]['average'] = array('#value' => $average_revenue);
      $sl_no++;
    }
  }

  $form['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#options' => $checkboxes,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send Coupons To Selected Customers'),
  );
  $form['#theme'] = 'my_form_theme';
  return $form;
}

/**
 * Setting form theme.
 */
function theme_my_form_theme($form) {
  $rows = array();
  foreach (element_children($form['checkboxes']) as $uid) {
    $row = array();
    $row[] = drupal_render($form['checkboxes'][$uid]);
    $row[] = drupal_render($form[$uid]['no']);
    $row[] = drupal_render($form[$uid]['name']);
    $row[] = drupal_render($form[$uid]['username']);
    $row[] = drupal_render($form[$uid]['orders']);
    $row[] = drupal_render($form[$uid]['products']);
    $row[] = drupal_render($form[$uid]['total']);
    $row[] = drupal_render($form[$uid]['average']);
    $rows[] = $row;
  }
  if (count($rows)) {
    $header = array(theme('table_select_header_cell'), t('#'), t('Customer'), t('Username'), t('Orders'), t('Products'),
      array('data' => t('Total'), 'field' => 'total', 'sort' => 'desc'), t('Average'));
  }
  else {
    $header = array(t('First Name'), t('Last Name'));
    $row = array();
    $row[] = array(
      'data' => t('No users were found'),
      'colspan' => 2,
      'style' => 'text-align:center',
    );
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  $output .= theme('pager', NULL, $page_size);
  $output .= drupal_render($form);
  return $output;
}
/**
 * Handling form submit.
 */
function uc_coupon_best_customers_form_submit($form_id, $form) {
  $selected_uids = $form['values']['checkboxes'];
  $discount_code = $form_values['coupon_code'];
  $site_name = variable_get('site_name', '');

  foreach ($selected_uids as $uid) {
    if ($uid != 0) {
      $res = db_query("SELECT name,mail FROM {users} WHERE uid =%d", $uid);
      while ($row = db_fetch_array($res)) {
        $mail_to = $row['mail'];
        $name = $row['name'];
      }
      $body = t("Dear $name,\n\n As you are selected as best customer list at $site_name\n we are providing coupon code to enjoy discount \n\n Your Discount Code is: $discount_code ");

      $message = array(
        'to' => $mail_to,
        'subject' => t('Best Customer Coupon Code From $site_name'),
        'body' => $body,
        'headers' => array('From' => 'info@sitename.com'),
      );
      $success = drupal_mail_send($message);
    }
  }
  if ($success) {
    drupal_set_message(t('Coupon sent successfully to customers email!'));
  }
  else {
    drupal_set_message(t('The coupon code not sent '));
  }
}
